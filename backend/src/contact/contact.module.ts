import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AuthModule } from 'src/auth/auth.module';
import { CompanyModule } from 'src/company/company.module';
import { ContactController } from './contact.controller';
import { ContactRepository } from './contact.repository';
import { ContactService } from './contact.service';

@Module({
  imports: [
    TypeOrmModule.forFeature([ContactRepository]),
    AuthModule,
    CompanyModule,
  ],
  controllers: [ContactController],
  providers: [ContactService],
})
export class ContactModule {}
