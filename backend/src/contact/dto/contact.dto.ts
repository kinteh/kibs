import { classToPlain, plainToClass } from 'class-transformer';
import { IsNotEmpty, IsOptional } from 'class-validator';
import { Company } from 'src/company/company.entity';
import { CompanyDto } from 'src/company/dto/company.dto';

export class ContactDto {
  @IsNotEmpty()
  name: string;

  @IsNotEmpty()
  lastName: string;

  @IsNotEmpty()
  telephone: string;

  // email is missing

  @IsOptional()
  company: CompanyDto;

  static toModel(companyDto: CompanyDto): Company {
    return plainToClass(Company, classToPlain(companyDto));
  }

  static toDto(company: Company): CompanyDto {
    return plainToClass(CompanyDto, classToPlain(company));
  }
}
