import { IsOptional, IsString } from 'class-validator';

export class GetContactFilterDto {
  @IsOptional()
  @IsString()
  name?: string;

  @IsOptional()
  @IsString()
  lastName?: string;
}
