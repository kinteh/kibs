import { InternalServerErrorException, Logger } from '@nestjs/common';
import { Company } from 'src/company/company.entity';
import { CompanyService } from 'src/company/company.service';
import { EntityRepository, Repository } from 'typeorm';
import { Contact } from './contact.entity';
import { ContactDto } from './dto/contact.dto';
import { GetContactFilterDto } from './dto/get-contact-filter.dto';

@EntityRepository(Contact)
export class ContactRepository extends Repository<Contact> {
  private logger = new Logger('ContactRepository');

  async getContacts(filterDto: GetContactFilterDto): Promise<Contact[]> {
    const { name, lastName } = filterDto;

    const query = this.createQueryBuilder('contact');

    if (name) {
      query.andWhere('(LOWER(contact.name) LIKE LOWER(:name))', {
        name: `%${name}%`,
      });
    }

    if (lastName) {
      query.andWhere('(LOWER(contact.lastName) LIKE LOWER(:lastName))', {
        name: `%${lastName}%`,
      });
    }

    try {
      const contacts = await query.getMany();
      return contacts;
    } catch (error) {
      this.logger.error(
        `Failed to get contacts with filters: ${JSON.stringify(filterDto)}`,
        error.stack,
      );
      throw new InternalServerErrorException();
    }
  }

  async createContact(contactDto: ContactDto): Promise<Contact> {
    const { name, lastName, telephone, company } = contactDto;

    const contact = this.create({
      name,
      lastName,
      telephone,
      company,
    });

    return await this.save(contact);
  }
}
