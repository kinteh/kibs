import {
  Body,
  Controller,
  Get,
  Logger,
  Param,
  Post,
  Query,
  UseGuards,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { CompanyService } from 'src/company/company.service';
import { Contact } from './contact.entity';
import { ContactService } from './contact.service';
import { ContactDto } from './dto/contact.dto';
import { GetContactFilterDto } from './dto/get-contact-filter.dto';

@Controller('contact')
@UseGuards(AuthGuard())
export class ContactController {
  private logger = new Logger('ContactController');

  constructor(private contactService: ContactService) {}

  @Get()
  getContacts(
    @Query() contactsFilterDto: GetContactFilterDto,
  ): Promise<Contact[]> {
    this.logger.verbose(
      `Retrieving all contacts. Filters: ${JSON.stringify(contactsFilterDto)}`,
    );

    return this.contactService.getContacts(contactsFilterDto);
  }

  @Get()
  getContactById(@Param('id') id: string): Promise<Contact> {
    return this.contactService.getContactById(id);
  }

  @Post()
  createContactAndCompany(@Body() contactDto: ContactDto): Promise<Contact> {
    this.logger.verbose(
      `Received new contact creation request. Data: ${JSON.stringify(
        contactDto,
      )}`,
    );

    return this.contactService.createContactAndCompany(contactDto);
  }
}
