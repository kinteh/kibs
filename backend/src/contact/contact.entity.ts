import { Exclude } from 'class-transformer';
import { Company } from 'src/company/company.entity';
import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class Contact {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column()
  name: string;

  @Column()
  lastName: string;

  @Column()
  telephone: string;

  // email is missing

  @ManyToOne((_type) => Company, (company) => company.contacts, {
    eager: false,
  })
  @Exclude({ toPlainOnly: true })
  company: Company;
}
