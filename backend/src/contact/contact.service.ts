import { Injectable, Logger } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { CompanyService } from 'src/company/company.service';
import { Contact } from './contact.entity';
import { ContactRepository } from './contact.repository';
import { ContactDto } from './dto/contact.dto';
import { GetContactFilterDto } from './dto/get-contact-filter.dto';

@Injectable()
export class ContactService {
  private logger = new Logger('ContactService');

  constructor(
    @InjectRepository(ContactRepository)
    private contactRepository: ContactRepository,
    private companyService: CompanyService,
  ) {}

  getContacts(filterDto: GetContactFilterDto): Promise<Contact[]> {
    return this.contactRepository.getContacts(filterDto);
  }

  getContactById(id: string): Promise<Contact> {
    return this.contactRepository.findOne(id);
  }

  async createContactAndCompany(contactDto: ContactDto): Promise<Contact> {
    this.logger.verbose(
      `Creating a new contact. Data: ${JSON.stringify(contactDto)}`,
    );

    if (contactDto.company?.name === 'undefined') {
      return this.contactRepository.createContact(contactDto);
    }

    const companyName = contactDto.company.name;

    this.logger.verbose(`Looking for company with name ${companyName}`);
    const found = await this.companyService.getCompanyByName(companyName);
    contactDto.company = found
      ? found
      : await this.companyService.createCompany(contactDto.company);

    return this.contactRepository.createContact(contactDto);
  }
}
