import {
  Body,
  Controller,
  Delete,
  Get,
  Logger,
  NotFoundException,
  Param,
  Patch,
  Post,
  Query,
  UseGuards,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { Company } from './company.entity';
import { CompanyService } from './company.service';
import { CompanyDto } from './dto/company.dto';
import { GetCompanyFilterDto } from './dto/get-company-filter.dto';
import { UpdateCompanyDto } from './dto/update-company.dto';

@Controller('company')
@UseGuards(AuthGuard())
export class CompanyController {
  private logger = new Logger('CompanyController');

  constructor(private companyService: CompanyService) {}

  @Get()
  getCompanies(@Query() filterDto: GetCompanyFilterDto): Promise<Company[]> {
    this.logger.verbose(
      `Retrieving all companies. Filters: ${JSON.stringify(filterDto)}`,
    );
    return this.companyService.getCompanies(filterDto);
  }

  @Get('/:id')
  getCompanyById(@Param('id') id: string): Promise<Company> {
    return this.companyService.getCompanyById(id);
  }

  @Post()
  createCompany(@Body() companyDto: CompanyDto): Promise<Company> {
    this.logger.verbose(
      `Creating a new company. Data: ${JSON.stringify(companyDto)}`,
    );
    return this.companyService.createCompany(companyDto);
  }

  @Delete('/:id')
  deleteCompany(@Param('id') id: string): Promise<void> {
    this.logger.verbose(`Deleting company with id ${id}`);
    return this.companyService.deleteCompany(id);
  }

  @Patch('/:id')
  updateCompany(
    @Param('id') id: string,
    @Body() updateCompanyDto: UpdateCompanyDto,
  ): Promise<void> {
    this.logger.verbose(
      `Updating company with id ${id} with following data: ${JSON.stringify(
        updateCompanyDto,
      )}`,
    );
    return this.companyService.updateCompany(id, updateCompanyDto);
  }
}
