import { Contact } from 'src/contact/contact.entity';
import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class Company {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column()
  name: string;

  @Column()
  address: string;

  @Column()
  oib: string;

  @OneToMany((_type) => Contact, (contact) => contact.company, {
    eager: true,
    cascade: ['insert', 'update'],
  })
  contacts: Contact[];
}
