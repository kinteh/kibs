import { IsNotEmpty } from 'class-validator';

export class CompanyDto {
  @IsNotEmpty()
  name: string;

  @IsNotEmpty()
  address: string;

  @IsNotEmpty()
  oib: string;
}
