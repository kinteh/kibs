import { InternalServerErrorException, Logger } from '@nestjs/common';
import { EntityRepository, Repository } from 'typeorm';
import { Company } from './company.entity';
import { CompanyDto } from './dto/company.dto';
import { GetCompanyFilterDto } from './dto/get-company-filter.dto';

@EntityRepository(Company)
export class CompanyRepository extends Repository<Company> {
  private logger = new Logger('TasksRepository');

  async getCompanies(filterDto: GetCompanyFilterDto): Promise<Company[]> {
    const { name } = filterDto;

    const query = this.createQueryBuilder('company');

    if (name) {
      query.andWhere('(LOWER(company.name) LIKE LOWER(:name))', {
        name: `%${name}%`,
      });
    }

    try {
      const companies = await query.getMany();
      return companies;
    } catch (error) {
      this.logger.error(
        `Failed to get companies with filters: ${JSON.stringify(filterDto)}`,
        error.stack,
      );
      throw new InternalServerErrorException();
    }
  }

  async createCompany(companyDto: CompanyDto): Promise<Company> {
    const { name, address, oib } = companyDto;

    const company = this.create({
      name,
      address,
      oib,
    });

    return await this.save(company);
  }
}
