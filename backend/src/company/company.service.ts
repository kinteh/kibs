import { Injectable, Logger, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Company } from './company.entity';
import { CompanyRepository } from './company.repository';
import { CompanyDto } from './dto/company.dto';
import { GetCompanyFilterDto } from './dto/get-company-filter.dto';
import { UpdateCompanyDto } from './dto/update-company.dto';

@Injectable()
export class CompanyService {
  private logger = new Logger('CompanyService');

  constructor(
    @InjectRepository(CompanyRepository)
    private companyRepository: CompanyRepository,
  ) {}

  async getCompanyById(id: string): Promise<Company> {
    const found = await this.companyRepository.findOne({ id });

    if (!found) {
      throw new NotFoundException(`Company with id: ${id} not found.`);
    }
    return found;
  }

  async getCompanyByName(name: string): Promise<Company> {
    const found = await this.companyRepository.findOne({ name });

    if (!found) {
      return null;
    }

    return found;
  }

  getCompanies(filterDto: GetCompanyFilterDto): Promise<Company[]> {
    return this.companyRepository.getCompanies(filterDto);
  }

  createCompany(companyDto: CompanyDto): Promise<Company> {
    this.logger.verbose(
      `Creating a new company. Data: ${JSON.stringify(companyDto)}`,
    );
    return this.companyRepository.createCompany(companyDto);
  }

  async deleteCompany(id: string): Promise<void> {
    const result = await this.companyRepository.delete({ id });

    if (result.affected === 0) {
      throw new NotFoundException(`Company with id: ${id} not found.`);
    }
  }

  async updateCompany(
    id: string,
    updateCompanyDto: UpdateCompanyDto,
  ): Promise<void> {
    const updatedCompany = await this.companyRepository.update(
      id,
      updateCompanyDto,
    );

    if (updatedCompany.affected === 0) {
      throw new NotFoundException(`Company with id: ${id} not found.`);
    }
  }
}
