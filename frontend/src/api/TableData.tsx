export interface TableData {
  id: string;
}
export interface Page<T> {
  data: T[];
  total: number;
}
