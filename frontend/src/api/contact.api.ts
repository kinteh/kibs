import Axios from 'axios';
import { authApi } from './auth';
import { TableData } from './TableData';

export interface IContact extends TableData {
  name: string;
  lastName: string;
  telephone: string;
  companyId: string;
}

export interface IContactCreate {
  name: string;
  lastName: string;
  telephone: string;
  companyId: string;
}
export interface IContactUpdate {
  id: string;
  name: string;
  lastName: string;
  telephone: string;
  companyId: string;
}

export class ContactApi {
  constructor(protected readonly baseUrl: string) {}

  async getAllContacts(): Promise<IContact[]> {
    const res = await Axios.get(`${this.baseUrl}`, {
      headers: authApi.getAuthHeaders(),
    });
    return res.data;
  }

  async create(contactCreate: IContactCreate): Promise<IContact> {
    const res = await Axios.post(this.baseUrl, contactCreate, {
      headers: authApi.getAuthHeaders(),
    });
    return res.data;
  }

  async delete(id: string): Promise<void> {
    await Axios.delete(`${this.baseUrl}/${id}`, {
      headers: authApi.getAuthHeaders(),
    });
  }

  async update(contactUpdate: IContactUpdate): Promise<IContact> {
    const res = await Axios.patch(
      `${this.baseUrl}/${contactUpdate.id}`,
      contactUpdate,
      {
        headers: authApi.getAuthHeaders(),
      },
    );
    return res.data;
  }
}

export const contactApi = new ContactApi('/api/contact');
