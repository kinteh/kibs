import Axios from 'axios';
import { authApi } from './auth';
import { TableData } from './TableData';

export interface ICompany extends TableData {
  name: string;
  address: string;
  oib: string;
}

export interface ICompanyCreate {
  name: string;
  address: string;
  oib: string;
}
export interface ICompanyUpdate {
  id: string;
  name: string;
  address: string;
  oib: string;
}

export class CompanyApi {
  constructor(protected readonly baseUrl: string) {}

  async getAllCompanies(): Promise<ICompany[]> {
    const res = await Axios.get(`${this.baseUrl}`, {
      headers: authApi.getAuthHeaders(),
    });
    return res.data;
  }

  async create(companyCreate: ICompanyCreate): Promise<ICompany> {
    const res = await Axios.post(this.baseUrl, companyCreate, {
      headers: authApi.getAuthHeaders(),
    });
    return res.data;
  }

  async delete(id: string): Promise<void> {
    await Axios.delete(`${this.baseUrl}/${id}`, {
      headers: authApi.getAuthHeaders(),
    });
  }

  async update(companyUpdate: ICompanyUpdate): Promise<ICompany> {
    const res = await Axios.patch(
      `${this.baseUrl}/${companyUpdate.id}`,
      companyUpdate,
      {
        headers: authApi.getAuthHeaders(),
      },
    );
    return res.data;
  }
}

export const companyApi = new CompanyApi('/api/company');
