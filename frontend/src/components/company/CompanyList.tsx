import AddIcon from '@mui/icons-material/Add';
import DeleteIcon from '@mui/icons-material/Delete';
import { Fab } from '@mui/material';
import Box from '@mui/material/Box';
import Button from '@mui/material/Button';
import Container from '@mui/material/Container';
import Paper from '@mui/material/Paper';
import Toolbar from '@mui/material/Toolbar';
import { DataGrid, GridColDef } from '@mui/x-data-grid';
import { useSnackbar } from 'notistack';
import React, { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';
import { companyApi } from '../../api/company.api';
import { TableData } from '../../api/TableData';
import {
  DEFAULT_ROWS_PER_PAGE,
  SX_PROPS_BOX_MAIN_COMPONENT,
} from '../common/Constants';
import Copyright from '../common/Copyright';
import HeaderSidebar from '../common/header-sidebar/HeaderSidebar';

function CompanyContent() {
  const { enqueueSnackbar } = useSnackbar();
  const [rows, setRows] = useState<TableData[]>([]);
  const [editRowsModel, setEditRowsModel] = React.useState({});
  const [editRowData, setEditRowData] = React.useState({});
  const [pageSize, setPageSize] = React.useState(5);

  const companyColumns: GridColDef[] = [
    { field: 'id', headerName: 'ID', flex: 1, editable: false },
    { field: 'name', headerName: 'name', flex: 1, editable: true },
    { field: 'address', headerName: 'address', flex: 1, editable: true },
    { field: 'oib', headerName: 'OIB', flex: 1, editable: true },
    {
      field: 'delete',
      headerName: 'Delete',
      sortable: false,
      flex: 0.5,
      align: 'center',
      renderCell: (params) => {
        return (
          <Button
            id={params.id.toString()}
            variant="contained"
            color="secondary"
            onClick={() => {
              handleRowDelete(params.id.toString());
            }}
            startIcon={<DeleteIcon />}
          >
            Delete
          </Button>
        );
      },
    },
  ];

  // Non-pro MUI DataGrid version does not allows usage of apiRef.
  // That's why we are using this workaround to be able to send the updated row to API.
  const handleEditRowsModelChange = React.useCallback(
    (model) => {
      const editedIds = Object.keys(model);

      // user stops editing when the edit model is empty
      if (editedIds.length === 0) {
        const companyId = Object.keys(editRowsModel)[0];
        const updatedCompanyDetails = mapUpdatedRowToObject(
          editRowData,
          companyId,
        );

        companyApi.update(createUpdatedCompany(updatedCompanyDetails));
      } else {
        setEditRowData(model[editedIds[0]]);
      }
      setEditRowsModel(model);
    },
    [editRowData, editRowsModel],
  );

  const createUpdatedCompany = (updatedCompanyDetails: any) => {
    return {
      id: updatedCompanyDetails.id,
      name: updatedCompanyDetails.name,
      address: updatedCompanyDetails.address,
      oib: updatedCompanyDetails.oib,
    };
  };

  const mapUpdatedRowToObject = (editRowData: any, companyId: string) => {
    let updatedObject: Map<string, string> = new Map<string, string>();

    updatedObject.set('id', companyId);

    Object.entries(editRowData).forEach((entry: any) => {
      updatedObject.set(entry[0], entry[1].value);
    });

    return Object.fromEntries(updatedObject);
  };

  const handleRowDelete = (companyId: string) => {
    companyApi
      .delete(companyId)
      .then(() => {
        getRows();
        enqueueSnackbar('Company deleted', {
          variant: 'success',
        });
      })
      .catch((error) => {
        enqueueSnackbar(
          `Error deleting company: ${error.response.data.message}`,
          {
            variant: 'error',
          },
        );
      });
  };

  const getRows = () => {
    companyApi
      .getAllCompanies()
      .then((response) => {
        setRows(response);
      })
      .catch((error) => {
        enqueueSnackbar(`Error retrieveing list of companies`, {
          variant: 'error',
        });
      });
  };

  useEffect(getRows, [enqueueSnackbar]);

  return (
    <>
      <HeaderSidebar />
      <Box component="main" sx={SX_PROPS_BOX_MAIN_COMPONENT}>
        <Toolbar />
        <Container maxWidth="lg" sx={{ mt: 4, mb: 4 }}>
          <Box sx={{ width: '100%' }}>
            <Paper sx={{ width: '100%', mb: 2 }}>
              <DataGrid
                editMode="row"
                rows={rows}
                columns={companyColumns}
                pageSize={pageSize}
                rowsPerPageOptions={DEFAULT_ROWS_PER_PAGE}
                autoHeight={true}
                onEditRowsModelChange={(editedRow, event) => {
                  handleEditRowsModelChange(editedRow);
                }}
                onPageSizeChange={(newPageSize) => setPageSize(newPageSize)}
                pagination
              />
            </Paper>
          </Box>
          <Link to="/company/new">
            <Fab color="primary" aria-label="add">
              <AddIcon />
            </Fab>
          </Link>
          <Copyright sx={{ pt: 4 }} />
        </Container>
      </Box>
    </>
  );
}

export default function Company() {
  return <CompanyContent />;
}
