import Box from '@mui/material/Box';
import Button from '@mui/material/Button';
import Container from '@mui/material/Container';
import Grid from '@mui/material/Grid';
import Paper from '@mui/material/Paper';
import TextField from '@mui/material/TextField';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import React from 'react';
import { companyApi } from '../../api/company.api';
import { useSnackbar } from 'notistack';
import { SX_PROPS_BOX_MAIN_COMPONENT } from '../common/Constants';
import Copyright from '../common/Copyright';
import HeaderSidebar from '../common/header-sidebar/HeaderSidebar';
import { useNavigate } from 'react-router-dom';

function CompanyNew() {
  const { enqueueSnackbar } = useSnackbar();
  const navigate = useNavigate();

  const handleSubmit = (event: React.FormEvent<HTMLFormElement>) => {
    event.preventDefault();
    const data = new FormData(event.currentTarget);
    const newCompany = {
      name: data.get('companyName')?.toString()!,
      address: data.get('companyAddress')?.toString()!,
      oib: data.get('companyOib')?.toString()!,
    };

    try {
      companyApi.create(newCompany);
      enqueueSnackbar('Successfully created new company.', {
        variant: 'success',
      });
      navigate('/company');
    } catch (error) {
      enqueueSnackbar('Could not create new company.', { variant: 'error' });
    }
  };

  return (
    <>
      <HeaderSidebar />
      <Box component="main" sx={SX_PROPS_BOX_MAIN_COMPONENT}>
        <Toolbar />
        <Container maxWidth="md" sx={{ mt: 4, mb: 4 }}>
          <Grid container spacing={3}>
            {/* Recent Orders */}
            <Grid item xs={12}>
              <Typography component="h1" variant="h5">
                Create new company
              </Typography>
              <Paper sx={{ p: 2, display: 'flex', flexDirection: 'column' }}>
                <Box
                  component="form"
                  noValidate
                  onSubmit={handleSubmit}
                  sx={{ mt: 3 }}
                >
                  <Grid container spacing={2}>
                    <Grid item xs={12} sm={6}>
                      <TextField
                        autoComplete="company-name"
                        name="companyName"
                        required
                        fullWidth
                        id="companyName"
                        label="Company Name"
                        autoFocus
                      />
                    </Grid>
                    <Grid item xs={12} sm={6}>
                      <TextField
                        required
                        fullWidth
                        id="oib"
                        label="OIB"
                        name="companyOib"
                        autoComplete="famcompanyily-name"
                      />
                    </Grid>
                    <Grid item xs={12}>
                      <TextField
                        required
                        fullWidth
                        id="companyAddress"
                        label="Company Address"
                        name="companyAddress"
                        autoComplete="companyAddress"
                      />
                    </Grid>
                  </Grid>
                  <Button
                    type="submit"
                    fullWidth
                    variant="contained"
                    sx={{ mt: 3, mb: 2 }}
                  >
                    Add company
                  </Button>
                </Box>
              </Paper>
            </Grid>
          </Grid>
          <Copyright sx={{ pt: 4 }} />
        </Container>
      </Box>
    </>
  );
}

export default function CompanyNewContent() {
  return <CompanyNew />;
}
