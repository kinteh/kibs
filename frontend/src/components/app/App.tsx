import { SnackbarProvider } from 'notistack';
import React from 'react';
import { BrowserRouter, Route, Routes } from 'react-router-dom';
import { authApi } from '../../api/auth';
import RequireAuth from '../common/RequireAuth';
import CompanyList from '../company/CompanyList';
import CompanyNew from '../company/CompanyNew';
import ContactList from '../contact/ContactList';
import ContactNew from '../contact/ContactNew';
import Dashboard from '../dashboard/Dashboard';
import Login from '../login/Login';
import Product from '../product/Product';
import './App.css';

const App = () => {
  return (
    <SnackbarProvider>
      <BrowserRouter>
        <Routes>
          <Route path="/login" element={<Login />} />
          <Route element={<RequireAuth />}>
            <Route path="/dashboard" element={<Dashboard />} />
            <Route path="/company" element={<CompanyList />} />
            <Route path="/company/new" element={<CompanyNew />} />
            <Route path="/contact" element={<ContactList />} />
            <Route path="/contact/new" element={<ContactNew />} />
            <Route path="/product" element={<Product />} />
          </Route>
          <Route
            path="/"
            element={authApi.isLoggedIn() ? <Dashboard /> : <Login />}
          />
        </Routes>
      </BrowserRouter>
    </SnackbarProvider>
  );
};

export default App;
