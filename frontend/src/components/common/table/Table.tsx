import Box from '@mui/material/Box';
import Paper from '@mui/material/Paper';
import { DataGrid, GridColDef } from '@mui/x-data-grid';
import * as React from 'react';
import { TableData } from '../../../api/TableData';

export default function DataTable(props: {
  rows: TableData[];
  columns: GridColDef[];
}) {
  return (
    <Box sx={{ width: '100%' }}>
      <Paper sx={{ width: '100%', mb: 2 }}>
        <DataGrid
          editMode="row"
          rows={props.rows}
          columns={props.columns}
          pageSize={5}
          rowsPerPageOptions={[5, 25, 50, 100]}
          autoHeight={true}
          onRowEditCommit={(values, event) => {
            console.log('kavalues: ', values);
          }}
        />
      </Paper>
    </Box>
  );
}
