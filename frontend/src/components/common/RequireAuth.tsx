import { useLocation, Navigate, Outlet } from 'react-router';
import { authApi } from '../../api/auth';

export default function RequireAuth() {
  const location = useLocation();

  if (!authApi.isLoggedIn()) {
    return <Navigate to="/login" state={{ from: location }} />;
  }

  return <Outlet />;
}
