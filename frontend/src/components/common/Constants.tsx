// SxProps for Box element
export const SX_PROPS_BOX_MAIN_COMPONENT = {
  backgroundColor: (theme: any) =>
    theme.palette.mode === 'light'
      ? theme.palette.grey[100]
      : theme.palette.grey[900],
  flexGrow: 1,
  height: '100vh',
  overflow: 'auto',
};

export const DEFAULT_ROWS_PER_PAGE = [5, 25, 50, 100];
