import Box from '@mui/material/Box';
import Button from '@mui/material/Button';
import Container from '@mui/material/Container';
import Grid from '@mui/material/Grid';
import Paper from '@mui/material/Paper';
import TextField from '@mui/material/TextField';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import { useSnackbar } from 'notistack';
import React from 'react';
import { useNavigate } from 'react-router-dom';
import { contactApi } from '../../api/contact.api';
import { SX_PROPS_BOX_MAIN_COMPONENT } from '../common/Constants';
import Copyright from '../common/Copyright';
import HeaderSidebar from '../common/header-sidebar/HeaderSidebar';

function ContactNew() {
  const { enqueueSnackbar } = useSnackbar();
  const navigate = useNavigate();

  const handleSubmit = (event: React.FormEvent<HTMLFormElement>) => {
    event.preventDefault();
    const data = new FormData(event.currentTarget);
    const newContact = {
      name: data.get('contactName')?.toString()!,
      lastName: data.get('contactName')?.toString()!,
      telephone: data.get('telephone')?.toString()!,
      companyId: data.get('companyId')?.toString()!,
    };

    try {
      contactApi.create(newContact);
      enqueueSnackbar('Successfully created new Contact.', {
        variant: 'success',
      });
      navigate('/contact');
    } catch (error) {
      enqueueSnackbar('Could not create new contact.', { variant: 'error' });
    }
  };

  return (
    <>
      <HeaderSidebar />
      <Box component="main" sx={SX_PROPS_BOX_MAIN_COMPONENT}>
        <Toolbar />
        <Container maxWidth="md" sx={{ mt: 4, mb: 4 }}>
          <Grid container spacing={3}>
            <Grid item xs={12}>
              <Typography component="h1" variant="h5">
                Create a new company
              </Typography>
              <Paper sx={{ p: 2, display: 'flex', flexDirection: 'column' }}>
                <Box
                  component="form"
                  noValidate
                  onSubmit={handleSubmit}
                  sx={{ mt: 3 }}
                >
                  <Grid container spacing={3}>
                    <Grid item xs={12} sm={6}>
                      <TextField
                        required
                        id="firstName"
                        name="firstName"
                        label="First name"
                        fullWidth
                        autoComplete="given-name"
                        variant="standard"
                      />
                    </Grid>
                    <Grid item xs={12} sm={6}>
                      <TextField
                        required
                        id="lastName"
                        name="lastName"
                        label="Last name"
                        fullWidth
                        autoComplete="family-name"
                        variant="standard"
                      />
                    </Grid>
                    <Grid item xs={12} sm={6}>
                      <TextField
                        required
                        id="telephone"
                        name="telephone"
                        label="Telephone"
                        fullWidth
                        autoComplete="shipping address-level2"
                        variant="standard"
                      />
                    </Grid>
                    <Grid item xs={12} sm={6}>
                      <TextField
                        id="state"
                        name="state"
                        label="Company"
                        value={'a dropdown menu should come here yeiyeyeye'}
                        fullWidth
                        variant="standard"
                      />
                    </Grid>
                  </Grid>
                  <Button
                    type="submit"
                    fullWidth
                    variant="contained"
                    sx={{ mt: 3, mb: 2 }}
                  >
                    Add contact
                  </Button>
                </Box>
              </Paper>
            </Grid>
          </Grid>
          <Copyright sx={{ pt: 4 }} />
        </Container>
      </Box>
    </>
  );
}

export default function ContactNewContent() {
  return <ContactNew />;
}
